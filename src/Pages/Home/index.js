import React, { useEffect, useState, useRef } from "react";
import { useStateValue } from "../../context/StateProvider";
import Score from "../../Components/Score";
import {
  HomeContainer,
  DifficultyLevel,
  SymbolList,
  Symbol,
  Button,
  Input,
} from "./styled";

function Home() {
  const [{ difficulty, listElement }, dispatch] = useStateValue();
  const [current, setCurrent] = useState({});
  const [status, setStatus] = useState("white");
  const input = useRef(null);

  useEffect(() => {
    const currentPos = listElement.findIndex(
      (element) => element.symbol === current.symbol
    );
    if (currentPos <= 0) {
      return;
    }
    let newCurrent;
    if (currentPos === -1) {
      newCurrent = listElement[listElement.length - 1];
    } else {
      newCurrent = listElement[currentPos - 1];
    }

    const timer = setTimeout(() => {
      setCurrent(newCurrent);
    }, newCurrent.time / difficulty);
    return () => {
      clearTimeout(timer);
    };
  }, [listElement, current, difficulty, dispatch]);

  function changeDifficulty(event) {
    const difficulty = event.target.dataset.difficulty || "1";
    dispatch({
      type: "CHANGE_DIFFICULTY",
      payload: parseInt(difficulty, 10),
    });
  }

  function startPlay() {
    dispatch({ type: "CLEAN_POINTS" });
    setStatus("white");
    setCurrent(listElement[listElement.length - 1]);
    input.current.focus();
  }

  function checkValue(e) {
    if (e.target.value === current.symbol) {
      setStatus("green");
      dispatch({
        type: "ADD_OK",
      });
    } else {
      setStatus("red");
      dispatch({
        type: "ADD_KO",
      });
    }
  }

  return (
    <HomeContainer>
      <h3>difficulty level = {difficulty}</h3>
      <button onClick={startPlay}>Start</button>
      <DifficultyLevel>
        <li>
          <Button
            $selectedLevel={difficulty === 1}
            data-difficulty="1"
            onClick={changeDifficulty}
          >
            Easy
          </Button>
        </li>
        <li>
          <Button
            $selectedLevel={difficulty === 2}
            data-difficulty="2"
            onClick={changeDifficulty}
          >
            Medium
          </Button>
        </li>
        <li>
          <Button
            $selectedLevel={difficulty === 3}
            data-difficulty="3"
            onClick={changeDifficulty}
          >
            Hard
          </Button>
        </li>
      </DifficultyLevel>
      <SymbolList>
        {listElement.map((element) => (
          <Symbol
            key={element.symbol}
            $current={current.symbol === element.symbol}
          >
            {element.symbol}
          </Symbol>
        ))}
      </SymbolList>
      <Input
        value=""
        id="letter"
        type="text"
        $status={status}
        onChange={checkValue}
        ref={input}
      />
      <Score />
    </HomeContainer>
  );
}

export default Home;
