import styled from "styled-components";

export const ListItem = styled.ul`
  display: flex;
  flex-direction: column;
`;

export const Item = styled.li`
  display: flex;
  padding: 0 5px;
  align-items: center;
`;

export const Text = styled.span`
  padding: 0 5px;
  :after {
    content: "-";
    padding-left: 5px;
  }
`;

export const Button = styled.button`
  border-radius: 5px;
  background: ${(props) => (props.$green ? "green" : "blue")};
  color: #fff;
  font-weight: bold;
  padding: 10px 20px;
  border: none;
`;

export const ContainerButton = styled.div`
  padding: 0 5px;
`;
