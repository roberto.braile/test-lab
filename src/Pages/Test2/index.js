import React from "react";
import List from "../../Components/List";
import AddItem from "../../Components/AddItem";

function Test2() {
  return (
    <div>
      <List />
      <AddItem />
    </div>
  );
}

export default Test2;
