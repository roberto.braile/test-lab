import React, { useState } from "react";
import { useStateValue } from "../../context/StateProvider";
import { ADD_OBJ } from "../../Const";
import { Container, ContainerActions } from "./styled";
import Form from "../Form";

function AddItem() {
  const [, dispatch] = useStateValue();
  const [showForm, setShowForm] = useState(false);
  const defaultItem = {
    imagen: "https://static.coincap.io/assets/icons/link@2x.png",
    reactions: ["default"],
  };

  function save(newObj) {
    handleShowForm(false);
    dispatch({
      type: ADD_OBJ,
      payload: newObj,
    });
  }

  function handleShowForm(value) {
    setShowForm(value);
  }

  function openForm() {
    setShowForm(true);
  }
  return (
    <Container>
      <ContainerActions>
        <button onClick={openForm}>Add New</button>
        <button>Cancel</button>
      </ContainerActions>
      {showForm && (
        <Form
          typeAdd
          handleShowForm={handleShowForm}
          item={defaultItem}
          action={save}
        />
      )}
    </Container>
  );
}

export default AddItem;
