import {
  CHANGE_DIFFICULTY,
  ADD_OK,
  ADD_KO,
  ADD_OBJ,
  CLEAN_POINTS,
  EDIT_OBJ,
  DELETE_OBJ,
} from "../Const";
export const initialState = {
  listElement: [
    {
      symbol: "a",
      time: 500,
    },
    {
      symbol: "@",
      time: 1500,
    },
    {
      symbol: "y",
      time: 2000,
    },
    {
      symbol: "x",
      time: 500,
    },
    {
      symbol: ".",
      time: 2000,
    },
    {
      symbol: "j",
      time: 1000,
    },
    {
      symbol: "-",
      time: 2000,
    },
    {
      symbol: "d",
      time: 500,
    },
    {
      symbol: "c",
      time: 2000,
    },
    {
      symbol: "q",
      time: 1000,
    },
    {
      symbol: "t",
      time: 2000,
    },
    {
      symbol: "p",
      time: 1000,
    },
    {
      symbol: "o",
      time: 2000,
    },
    {
      symbol: "f",
      time: 500,
    },
    {
      symbol: ",",
      time: 2000,
    },
    {
      symbol: "+",
      time: 1000,
    },
  ],
  difficulty: 1,
  ok: 0,
  ko: 0,
  objList: [
    {
      id: 1,
      imagen: "https://static.coincap.io/assets/icons/btc@2x.png",
      title: "bitcoin",
      text: "bitcoin coin description",
      state: "enabled",
      reactions: ["default"],
    },
    {
      id: 2,
      imagen: "https://static.coincap.io/assets/icons/eth@2x.png",
      title: "Ethereum",
      text: "Ethereum coin description",
      state: "enabled",
      reactions: ["default"],
    },
    {
      id: 3,
      imagen: "https://static.coincap.io/assets/icons/usdt@2x.png",
      title: "ticoin",
      text: "Tether coin description",
      state: "enabled",
      reactions: [],
    },
    {
      id: 4,
      imagen: "https://static.coincap.io/assets/icons/xrp@2x.png",
      title: "xrpcoin",
      text: "XRP coin description",
      state: "enabled",
      reactions: [],
    },
  ],
};

const reducer = (state, action) => {
  switch (action.type) {
    case CHANGE_DIFFICULTY:
      return {
        ...state,
        difficulty: action.payload,
      };

    case ADD_OK:
      return {
        ...state,
        ok: state.ok + 1,
      };

    case ADD_KO:
      return {
        ...state,
        ko: state.ko + 1,
      };

    case ADD_OBJ:
      return {
        ...state,
        objList: [
          ...state.objList,
          {
            ...action.payload,
            id: state.objList.length + 1,
          },
        ],
      };

    case DELETE_OBJ:
      const filteredList = state.objList.filter(
        (item) => item.id !== parseInt(action.payload)
      );
      return {
        ...state,
        objList: filteredList,
      };

    case CLEAN_POINTS:
      return {
        ...state,
        ko: 0,
        ok: 0,
      };

    case EDIT_OBJ:
      const newList = state.objList.map((item) =>
        item.id !== action.payload.id ? item : action.payload
      );
      return {
        ...state,
        objList: [...newList],
      };

    default:
      return state;
  }
};

export default reducer;
