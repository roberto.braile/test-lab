import React, { useState, useEffect } from "react";
import { Button, ContainerButton, EditForm, ContainerField } from "./styled";

function Form({ handleShowForm, item, action, typeAdd }) {
  const [newObj, setNewObj] = useState(item);

  useEffect(() => {
    setNewObj(item);
  }, [item]);

  function handleAction() {
    action(newObj);
    handleShowForm(false);
  }

  function cancel() {
    handleShowForm(false);
  }

  function handleChange(e) {
    const { value, name } = e.target;
    setNewObj({
      ...newObj,
      [name]: value,
    });
  }

  function selectReaction(e) {
    const { value } = e.target;
    let newReact = newObj.reactions ? [...newObj.reactions] : [];
    const pos = newReact.indexOf(value);
    if (pos > -1) {
      newReact.splice(pos, 1);
    } else {
      newReact.push(value);
    }

    setNewObj({
      ...newObj,
      reactions: newReact,
    });
  }

  return (
    <div>
      <EditForm>
        {typeAdd && (
          <ContainerField>
            <label>Url Image:</label>
            <input
              name="imagen"
              onChange={handleChange}
              type="text"
              value={newObj.imagen || ""}
            />
          </ContainerField>
        )}
        <ContainerField>
          <label>Title:</label>
          <input
            name="title"
            onChange={handleChange}
            type="text"
            value={newObj.title || ""}
          />
        </ContainerField>

        <ContainerField>
          <label>Text:</label>
          <input
            name="text"
            onChange={handleChange}
            type="text"
            value={newObj.text || ""}
          />
        </ContainerField>

        <ContainerField>
          <label>State:</label>
          <input
            name="state"
            onChange={handleChange}
            type="text"
            value={newObj.state || ""}
          />
        </ContainerField>
        <ContainerField>
          <p>
            {newObj.reactions?.map((reaction) => (
              <span key={reaction}>{reaction}-</span>
            ))}
          </p>
        </ContainerField>
        <ContainerField>
          <select name="reactions" onChange={selectReaction}>
            <option value="default">default</option>
            <option value="opt1">opt1</option>
            <option value="opt2">opt2</option>
            <option value="opt3">opt3</option>
            <option value="opt4">opt4</option>
          </select>
        </ContainerField>
        <ContainerButton>
          <Button $green onClick={handleAction}>
            Save
          </Button>
          <Button $green onClick={cancel}>
            Cancel
          </Button>
        </ContainerButton>
      </EditForm>
    </div>
  );
}

export default Form;
