export const CHANGE_DIFFICULTY = "CHANGE_DIFFICULTY";
export const ADD_OK = "ADD_OK";
export const ADD_KO = "ADD_KO";
export const ADD_OBJ = "ADD_OBJ";
export const CLEAN_POINTS = "CLEAN_POINTS";
export const EDIT_OBJ = "EDIT_OBJ";
export const DELETE_OBJ = "DELETE_OBJ";
