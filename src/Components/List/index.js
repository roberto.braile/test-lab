import React, { useState } from "react";
import { useStateValue } from "../../context/StateProvider";
import { EDIT_OBJ, DELETE_OBJ } from "../../Const";
import Form from "../Form";
import { ListItem, Item, Text, Button, ContainerButton } from "./styled";

export default function List() {
  const [{ objList }, dispatch] = useStateValue();
  const [showEdit, setShowEdit] = useState(false);
  const [item, setItem] = useState({});

  function edit(e) {
    const id = e.target.dataset.id;
    const itemObj = objList.find((obj) => {
      return parseInt(obj.id, 10) === parseInt(id, 10);
    });
    setShowEdit(true);
    setItem(itemObj);
  }

  function save(newObj) {
    handleShowForm(false);
    dispatch({
      type: EDIT_OBJ,
      payload: newObj,
    });
  }

  function deleteItem(e) {
    dispatch({
      type: DELETE_OBJ,
      payload: e.target.dataset.id,
    });
  }

  function handleShowForm(value) {
    setShowEdit(value);
  }

  return (
    <>
      {showEdit && (
        <Form handleShowForm={handleShowForm} item={item} action={save} />
      )}
      <ListItem>
        {objList.map((item) => (
          <Item key={item.title + item.id}>
            <img src={item.imagen} alt={item.title} />
            <Text>{item.id}</Text>
            <Text>{item.title}</Text>
            <Text>{item.text}</Text>
            <Text>{item.state}</Text>
            <Text>
              {item.reactions.map((reaction) => (
                <span key={reaction}>{reaction}-</span>
              ))}
            </Text>
            <ContainerButton>
              <Button data-id={item.id} onClick={edit}>
                Edit
              </Button>
              <Button data-id={item.id} onClick={deleteItem}>
                Delete
              </Button>
            </ContainerButton>
          </Item>
        ))}
      </ListItem>
    </>
  );
}
