import styled from 'styled-components';

export const HomeContainer = styled.div`
  display: flex;
  flex-direction: column;
  background: #f4f4f4;
`;

export const DifficultyLevel = styled.ul`
  display: flex;
  list-style: none;
`;

export const SymbolList = styled.ul`
  display: flex;
  list-style: none;
`;

export const Symbol = styled.li`
  width: 50px;
  height: 50px;
  line-height: 50px;
  text-align: center;
  border: 1px solid #ccc;
  background: ${(props) => (props.$current ? 'green' : 'white')};
  :hover {
    cursor: pointer;
  }
`;

export const Button = styled.button`
  background: ${(props) => (props.$selectedLevel ? 'green' : 'white')};
`;

export const Input = styled.input`
  width: 200px;
  height: 40px;
  background: ${(props) => props.$status};
`;
