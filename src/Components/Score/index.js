import React from 'react';
import { useStateValue } from '../../context/StateProvider';

const Score = () => {
  const [{ ok, ko }] = useStateValue();
  return (
    <>
      <h2>Score</h2>
      <h4>ok: {ok}</h4>
      <h4>ko: {ko}</h4>
    </>
  );
};

export default Score;
