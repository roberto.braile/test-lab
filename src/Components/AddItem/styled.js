import styled from "styled-components";

export const Container = styled.div`
  padding: 10px;
`;

export const ContainerActions = styled.div`
  padding: 20px 0;
`;
