import React from 'react';
import { Link } from 'react-router-dom';
import { ContainerHeader, Menu, MenuItem } from './styled';

function Header() {
  return (
    <ContainerHeader>
      <p>HEADER</p>
      <Menu>
        <MenuItem>
          <Link to="/">Home</Link>
        </MenuItem>
        <MenuItem>
          <Link to="/test2">Test2</Link>
        </MenuItem>
      </Menu>
    </ContainerHeader>
  );
}

export default Header;
