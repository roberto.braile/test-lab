import styled from "styled-components";

export const ContainerFooter = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  background: #f4f4f4;
`;
