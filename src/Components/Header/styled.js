import styled from 'styled-components';

export const ContainerHeader = styled.div`
  display: flex;
  width: 100%;
  background: #f4f4f4;
`;

export const Menu = styled.ul`
  display: flex;
  list-style: none;
`;

export const MenuItem = styled.li`
  a {
    text-decoration: none;
    padding: 5px;
    :hover {
      text-decoration: underline;
    }
  }
`;
