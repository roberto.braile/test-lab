import styled from "styled-components";

export const Button = styled.button`
  border-radius: 5px;
  background: ${(props) => (props.$green ? "green" : "blue")};
  color: #fff;
  font-weight: bold;
  padding: 10px 20px;
  border: none;
  margin-right: 10px;
`;

export const ContainerButton = styled.div`
  padding: 0 5px;
`;

export const EditForm = styled.div`
  display: flex;
  align-items: center;
`;

export const ContainerField = styled.div`
  padding-right: 10px;
`;
